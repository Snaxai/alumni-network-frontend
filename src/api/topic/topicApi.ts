import { createAuthenticatedHeader, createHeader } from "../helperFunctions";
import { ITopic, ITopicMembershipRequest } from "./topicTypes";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

/**
 * Fetches all topics
 * @returns topics
 */
export const fetchTopics = async () => {
	try {
		const response = await fetch(`${apiUrl}/topic`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch topics");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const fetchTopicById = async (topicId: number) => {
	try {
		const response = await fetch(`${apiUrl}/topic/${topicId}`);
		if (!response.ok)
			throw new Error("Could not fetch topic with id: " + topicId);
		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

export const createTopic = async (topic: ITopic) => {
	try {
		const response = await fetch(`${apiUrl}/topic`, {
			method: "POST",
			headers: createHeader(),
			body: JSON.stringify({
				topic,
			}),
		});
		if (!response.ok) throw new Error("Could not create topic");
		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Subscribes to a topic
 * @param req contains topic id and user id
 * @returns 
 */
export const createTopicMembership = async (req: ITopicMembershipRequest) => {
	try {
		const response = await fetch(`${apiUrl}/topic/${req.topicId}/join`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.userId),
		});
		if (!response.ok) throw new Error("Could not create topic membership");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Unsubscribe to a topic
 * @param req contains topic id and user id
 * @returns 
 */
export const deleteTopicMembership = async (req: ITopicMembershipRequest) => {
	try {
		const response = await fetch(`${apiUrl}/topic/${req.topicId}/remove`, {
			method: "DELETE",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.userId),
		});
		if (!response.ok) throw new Error("Could not delete topic membership");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		console.log("ERROR UNSUBSCRIBING");
		return error.message;
	}
};
