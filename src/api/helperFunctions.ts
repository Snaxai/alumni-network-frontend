export let authenticationToken: string;

/**
 * Creates default headers without authentication
 * @returns headers
 */
export const createHeader = () => {
	return {
		"Content-Type": "application/json",
		Accept: "application/json",
	};
};

/**
 * Creates default headers with authentication
 * @returns headers
 */
export const createAuthenticatedHeader = () => {
	return {
		"Content-Type": "application/json",
		Accept: "application/json",
		Authorization: "bearer " + authenticationToken,
	};
};

export const addAuthToken = (authToken: string) => {
	authenticationToken = authToken;
};

export const handleResponse = () => {};
