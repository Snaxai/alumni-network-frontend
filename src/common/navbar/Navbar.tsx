import React from "react";
import {
	AppBar,
	Avatar,
	Button,
	Grid,
	IconButton,
	Menu,
	MenuItem,
	Toolbar,
	Tooltip,
	Typography,
} from "@mui/material";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
	logoutAction,
	selectProfilePicture,
} from "../../features/user/userSlice";
import { useAppSelector } from "../../app/hooks";
import { CalendarMonthOutlined } from "@mui/icons-material";
import GroupList from "../../features/group/GroupList";
import TopicList from "../../features/topic/TopicList";
import { useAuth0 } from "@auth0/auth0-react";
import { setLoginMessage } from "../../features/login/loginSlice";

const Navbar = () => {
	const dispatch = useDispatch();
	const profilePicture = useAppSelector(selectProfilePicture);
	const navigate = useNavigate();
	const { logout } = useAuth0();

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	const [groupAnchorEl, setGroupAnchorEl] =
		React.useState<HTMLButtonElement | null>(null);
	const [topicAnchorEl, setTopicAnchorEl] =
		React.useState<HTMLButtonElement | null>(null);

	/**
	 * Opens the profile menu
	 * @param event
	 */
	const handleProfileMenu = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleProfileMenuClose = () => {
		setAnchorEl(null);
	};

	/**
	 * logs the user out of the app and resets states
	 */
	const handleLogoutClick = () => {
		setAnchorEl(null);
		logout(
			window.location.href.includes("localhost")
				? { returnTo: "http://localhost:3000/" }
				: { returnTo: "https://alumni-network-frontend.herokuapp.com/" }
		);
		dispatch(logoutAction());
		dispatch(setLoginMessage("You have been logged out"));
	};

	const onCalendarClick = ({}) => {
		navigate("/calendar");
	};

	/**
	 * Opens the group popover list
	 * @param event
	 */
	const handleGroupClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		setGroupAnchorEl(event.currentTarget);
	};

	/**
	 * Opens the topic popover list
	 * @param event
	 */
	const handleTopicClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		setTopicAnchorEl(event.currentTarget);
	};

	/**
	 * Closes the popover list(s)
	 */
	const handleClose = () => {
		setGroupAnchorEl(null);
		setTopicAnchorEl(null);
	};

	return (
		<AppBar position="static" sx={{ mb: 2 }}>
			<Toolbar>
				<Grid container>
					<Avatar
						onClick={() => navigate("/timeline")}
						sx={{ bgcolor: "white", mr: 1, "&:hover": { cursor: "pointer" } }}
						src="https://icon-library.com/images/network-icon-png/network-icon-png-20.jpg"
					/>
					<Tooltip title="Go to timeline">
						<MenuItem
							key="timeline"
							sx={{ borderRadius: 2 }}
							component={Button}
							onClick={() => navigate("/timeline")}>
							Timeline
						</MenuItem>
					</Tooltip>
					<Tooltip title="Show groups">
						<MenuItem
							key="group"
							component={Button}
							sx={{ borderRadius: 2 }}
							/*sx={{
								fontWeight: "bold",
								color: "black",
								bgcolor: "lightblue",
								borderRadius: 2,
								"&:hover": { bgcolor: "lightgray" },
							}}*/
							onClick={handleGroupClick}>
							Group
						</MenuItem>
					</Tooltip>
					<GroupList
						open={Boolean(groupAnchorEl)}
						handleClose={handleClose}
						anchorEl={groupAnchorEl}
					/>
					<Tooltip title="Show topics">
						<MenuItem
							sx={{ borderRadius: 2 }}
							key="topics"
							component={Button}
							onClick={handleTopicClick}>
							Topics
						</MenuItem>
					</Tooltip>
					<TopicList
						open={Boolean(topicAnchorEl)}
						handleClose={handleClose}
						anchorEl={topicAnchorEl}
					/>
				</Grid>

				<Grid container justifyContent="flex-end">
					<Grid item sx={{ mr: 2 }}>
						<IconButton
							size="large"
							aria-haspopup="true"
							onClick={onCalendarClick}>
							<CalendarMonthOutlined
								sx={{ color: "black", width: 40, height: 40 }}
							/>
						</IconButton>
					</Grid>
					<IconButton
						aria-label="account of current user"
						aria-controls="menu-appbar"
						aria-haspopup="true"
						onClick={handleProfileMenu}
						color="inherit">
						<Avatar src={profilePicture} />
					</IconButton>
					<Menu
						id="menu-appbar"
						anchorEl={anchorEl}
						anchorOrigin={{
							vertical: "top",
							horizontal: "right",
						}}
						keepMounted
						transformOrigin={{
							vertical: "top",
							horizontal: "right",
						}}
						open={Boolean(anchorEl)}
						onClose={handleProfileMenuClose}>
						<MenuItem
							onClick={handleProfileMenuClose}
							component={NavLink}
							to="/settings">
							Profile
						</MenuItem>
						<MenuItem onClick={handleLogoutClick} sx={{ color: "red" }}>
							Logout
						</MenuItem>
					</Menu>
				</Grid>
			</Toolbar>
		</AppBar>
	);
};

export default Navbar;
