import {
	Grid,
	TextField,
	Button,
	Backdrop,
	Box,
	Fade,
	Modal,
	Typography,
	MenuItem,
	Snackbar,
	Alert,
	Tooltip,
	Divider,
} from "@mui/material";
import { useState } from "react";
import { IPostBody } from "../../api/post/postApiTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { SnackBarSeverity } from "../../common/commonTypes";
import {
	selectGroupById,
	selectGroupIds,
	selectPublicAndPartOfGroups,
} from "../group/groupSlice";
import { selectTopicById, selectTopicIds } from "../topic/topicSlice";
import { selectUserId } from "../user/userSlice";
import { createPost } from "./postSlice";

interface postProps {
	openPost: boolean;
	handleClosePost: (openPost: boolean) => void;
}

const PostModal = (props: postProps) => {
	const dispatch = useAppDispatch();

	// Local States
	const [postTitle, setPostTitle] = useState<string>("");
	const [postGroup, setPostGroup] = useState<string>("");
	const [postTopics, setPostTopics] = useState<number[]>();
	const [postTopicName, setPostTopicName] = useState("");
	const [content, setContent] = useState<string>("");
	const [openSnack, setOpenSnack] = useState(false);
	const [snackText, setSnackText] = useState<string | undefined>();
	const [severity, setSeverity] = useState<SnackBarSeverity>("error");

	// Hooks
	const userId = useAppSelector(selectUserId);
	const groupsIds = useAppSelector(selectPublicAndPartOfGroups);
	const groupById = useAppSelector(selectGroupById);
	const topics = useAppSelector(selectTopicIds);
	const topicById = useAppSelector(selectTopicById);

	const requiredFieldsFilledCorrect =
		postTitle.length < 3 || content.length < 3;

	const handlePostTitleChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setPostTitle(event.target.value);
	};

	const handlePostGroupChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setPostGroup(event.target.value);
	};

	const handlePostTopicChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setPostTopics([parseInt(event.target.value)]);
		setPostTopicName(event.target.value);
	};

	const handleContentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setContent(event.target.value);
	};

	const cancelClick = () => {
		props.handleClosePost(false);
	};

	const handleClose = (
		event?: React.SyntheticEvent | Event,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenSnack(false);
	};

	const resetLocalStates = () => {
		setPostTitle("");
		setContent("");
		setPostTopicName("");
		setPostTopics(undefined);
		setPostGroup("");
	};

	const createPostClick = () => {
		const topics: { topicId: number }[] = [];
		postTopics &&
			postTopics.forEach((id) => {
				const topic = { topicId: id };
				topics.push(topic);
			});
		const post: IPostBody = {
			lastUpdated: "",
			title: postTitle,
			senderId: userId,
			postMessage: content,
			target_group: postGroup
				? {
						groupId: parseInt(postGroup),
				  }
				: undefined,
			target_topics: topics,
		};
		dispatch(createPost(post))
			.unwrap()
			.then(() => {
				props.handleClosePost(false);
				setSnackText("Post successfully Created!");
				setOpenSnack(true);
				setSeverity("success");
				resetLocalStates();
			})
			.catch((error) => {
				console.log(error);
				setSnackText("Error: Something went wrong");
				setOpenSnack(true);
				setSeverity("error");
			});
	};

	const style = {
		position: "absolute" as "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 600,
		bgcolor: "white",
		boxShadow: 24,
		p: 4,
		borderRadius: 2,
	};

	return (
		<>
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				open={props.openPost}
				onClose={props.handleClosePost}
				closeAfterTransition
				BackdropComponent={Backdrop}>
				<Fade in={props.openPost}>
					<Box sx={style}>
						<Typography
							id="transition-modal-title"
							variant="h5"
							textAlign="center">
							Create a new post
						</Typography>
						<Divider variant="middle" />
						<Typography variant="subtitle1" textAlign="center">
							Title and post content must be filled out
						</Typography>
						<Grid container>
							<TextField
								required
								margin="normal"
								fullWidth
								id="title"
								label="Title"
								name="Title"
								value={postTitle}
								onChange={handlePostTitleChange}
							/>
							<TextField
								select
								margin="normal"
								fullWidth
								name="group"
								label="Group"
								id="group"
								value={postGroup}
								onChange={handlePostGroupChange}>
								{groupsIds.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {groupById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								select
								margin="normal"
								fullWidth
								name="topic"
								label="Topic"
								id="topic"
								value={postTopicName}
								onChange={handlePostTopicChange}>
								{topics.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {topicById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								required
								margin="normal"
								fullWidth
								name="postContent"
								label="Post Content"
								id="postContent"
								multiline
								rows={10}
								defaultValue={content}
								onChange={handleContentChange}
							/>
							<Grid container justifyContent="space-between" marginTop={2}>
								<Grid item>
									<Button
										color="error"
										variant="contained"
										onClick={cancelClick}>
										Cancel
									</Button>
								</Grid>

								<Grid item>
									<Tooltip
										arrow={true}
										title={
											requiredFieldsFilledCorrect
												? "Title and post must contain 3 or more letters"
												: "Create Post"
										}>
										<span>
											<Button
												variant="contained"
												disabled={requiredFieldsFilledCorrect}
												onClick={createPostClick}>
												Create
											</Button>
										</span>
									</Tooltip>
								</Grid>
							</Grid>
						</Grid>
					</Box>
				</Fade>
			</Modal>
			{snackText && (
				<Snackbar
					open={openSnack}
					autoHideDuration={6000}
					onClose={handleClose}>
					<Alert
						onClose={handleClose}
						severity={severity}
						sx={{
							width: "100%",
							fontSize: 20,
							bgcolor: severity === "error" ? "red" : "lightgreen",
						}}>
						{snackText}
					</Alert>
				</Snackbar>
			)}
		</>
	);
};

export default PostModal;
