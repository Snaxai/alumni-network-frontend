import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
} from "@mui/material";
import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { setActiveEventId } from "../event/eventSlice";
import {
	selectFilteredAndSortedByLastUpdatedPostIds,
	selectPostById,
	setActivePostId,
} from "../post/postSlice";
import { columns } from "./Timeline";
import TimelinePostItem from "./TimelinePostItem";

/**
 * The timeline post list. Shows when the tab is set to "Posts"
 * @returns Timeline post list
 */
const TimelinePosts = () => {
	const dispatch = useAppDispatch();

	// Selectors
	const postsIds = useAppSelector(selectFilteredAndSortedByLastUpdatedPostIds);
	const post = useAppSelector(selectPostById);

	const handleTimelinePostItemClick = (id: string | undefined) => {
		dispatch(setActiveEventId(undefined));
		dispatch(setActivePostId(id));
	};

	return (
		<TableContainer sx={{ maxHeight: 640 }}>
			<Table stickyHeader aria-label="sticky table">
				<TableHead>
					<TableRow>
						{columns.map((column) => (
							<TableCell
								sx={{ fontWeight: "bold" }}
								key={column.id}
								align={column.align}
								style={{ minWidth: column.minWidth }}>
								{column.label}
							</TableCell>
						))}
					</TableRow>
				</TableHead>
				<TableBody>
					{postsIds.map((id) =>
						post[parseInt(id)].postTarget ||
						post[parseInt(id)].target_event ? undefined : (
							<TimelinePostItem
								key={`post${id}`}
								handleTimelinePostItemClick={handleTimelinePostItemClick}
								id={id}
							/>
						)
					)}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

export default TimelinePosts;
