import {
	TableContainer,
	Table,
	TableHead,
	TableRow,
	TableCell,
	TableBody,
} from "@mui/material";
import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectFilteredEventIds, setActiveEventId } from "../event/eventSlice";
import {
	selectFilteredAndSortedByLastUpdatedPostIds,
	selectPostById,
	setActivePostId,
} from "../post/postSlice";
import { columns } from "./Timeline";
import TimelineEventItem from "./TimelineEventItem";
import TimelinePostItem from "./TimelinePostItem";

/**
 * The timeline event list. Shows when the tab is set to "Events"
 * @returns Timeline event list
 */
const TimelineEvents = () => {
	const dispatch = useAppDispatch();

	// Selectors
	const eventIds = useAppSelector(selectFilteredEventIds);

	const handleTimelineEventItemClick = (id: string | undefined) => {
		dispatch(setActivePostId(undefined));
		dispatch(setActiveEventId(id));
	};

	return (
		<TableContainer sx={{ maxHeight: 640 }}>
			<Table stickyHeader aria-label="sticky table" sx={{ margin: 0, p: 0 }}>
				<TableHead>
					<TableRow>
						{columns.map((column) => (
							<TableCell
								sx={{ fontWeight: "bold" }}
								key={column.id}
								align={column.align}
								style={{
									minWidth: column.minWidth,
								}}>
								{column.label}
							</TableCell>
						))}
					</TableRow>
				</TableHead>
				<TableBody>
					{eventIds.map((id) => (
						<TimelineEventItem
							key={`event${id}`}
							id={id}
							handleTimelineEventItemClick={handleTimelineEventItemClick}
						/>
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

export default TimelineEvents;
