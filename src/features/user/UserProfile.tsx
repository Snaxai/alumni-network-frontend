import { Container, Box, Avatar, Typography, TextField } from "@mui/material";
import React from "react";
import { useAppSelector } from "../../app/hooks";
import { selectCurrentUserId } from "../allUsers/usersSlice";

/**
 * The user profile component for other users in the app
 * @returns the user profile component
 */
const UserProfile = () => {
	const userId = useAppSelector(selectCurrentUserId);
	const currentUser = useAppSelector(
		(state) => state.allUsers.userById[userId ? parseInt(userId) : 0]
	);
	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}>
				<Avatar
					sx={{
						m: 1,
						bgcolor: "primary.main",
						width: 130,
						height: 130,
					}}
					src={currentUser.picture}
				/>
				<Typography component="h1" variant="h5">
					{currentUser.name}
				</Typography>

				<TextField
					margin="normal"
					fullWidth
					name="workStatus"
					label="Work Status"
					id="workStatus"
					multiline
					InputProps={{
						readOnly: true,
					}}
					value={currentUser.status}
				/>
				<TextField
					margin="normal"
					fullWidth
					name="bio"
					label="Bio or Short Resume"
					id="bio"
					multiline
					rows={5}
					InputProps={{
						readOnly: true,
					}}
					value={currentUser.bio}
				/>
				<TextField
					margin="normal"
					fullWidth
					multiline
					name="funFact"
					label="Fun Fact"
					id="funFact"
					InputProps={{
						readOnly: true,
					}}
					value={currentUser.funFact}
				/>
			</Box>
		</Container>
	);
};

export default UserProfile;
