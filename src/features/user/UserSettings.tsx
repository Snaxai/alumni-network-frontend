import {
	Container,
	CssBaseline,
	Box,
	Avatar,
	Typography,
	TextField,
	Button,
} from "@mui/material";
import React, { useState } from "react";
import { IUpdateUser, IUserInfo } from "../../api/user/userTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { DataStatus } from "../../common/commonTypes";
import { setAllUsersStatus } from "../allUsers/usersSlice";
import {
	selectBio,
	selectName,
	selectFunFact,
	selectProfilePicture,
	selectWorkStatus,
	updateUserInfo,
	selectUserId,
	fetchUserInfo,
	fetchUserInfoWithToken,
} from "./userSlice";

/**
 * The logged in profile
 * @returns the profile for the logged in user
 */
const UserSettings = () => {
	const dispatch = useAppDispatch();

	const username = useAppSelector(selectName);
	const bio = useAppSelector(selectBio);
	const profilePicture = useAppSelector(selectProfilePicture);
	const workStatus = useAppSelector(selectWorkStatus);
	const funFact = useAppSelector(selectFunFact);
	const userId = useAppSelector(selectUserId);

	const [name, setName] = useState<string>(username);
	const [biography, setBiography] = useState<string>(bio);
	const [status, setStatus] = useState<string>(workStatus);
	const [fact, setFact] = useState<string>(funFact);
	const [picture, setPicture] = useState<string>(profilePicture);
	const [message, setMessage] = useState("");

	const requiredInputsFilled =
		name === username &&
		biography === bio &&
		status === workStatus &&
		fact === funFact &&
		picture === profilePicture;

	const handleUpdateUser = () => {
		if (!requiredInputsFilled) {
			const userInfo: IUpdateUser = {
				id: userId,
				body: {
					bio: biography,
					funFact: fact,
					name,
					picture,
					workStatus: status,
				},
			};
			dispatch(updateUserInfo(userInfo))
				.then(() => {
					dispatch(fetchUserInfoWithToken());
					setMessage("Successfully updated user");
					dispatch(setAllUsersStatus(DataStatus.Idle));
				})
				.catch((error) => {
					console.log(error);
				});
		} else setMessage("No changes has been made");
	};

	const handleNameChange = (event: any) => {
		setName(event.target.value);
	};

	const handlePictureUrlChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setPicture(event.target.value);
	};

	const handleBioChange = (event: any) => {
		setBiography(event.target.value);
	};

	const handleStatusChange = (event: any) => {
		setStatus(event.target.value);
	};

	const handleFactChange = (event: any) => {
		setFact(event.target.value);
	};

	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}>
				<Avatar
					sx={{
						m: 1,
						bgcolor: "primary.main",
						width: 130,
						height: 130,
					}}
					src={profilePicture}
				/>
				<Typography component="h1" variant="h5">
					{username}
				</Typography>
				<Box
					component="form"
					noValidate
					onSubmit={handleUpdateUser}
					sx={{ mt: 2 }}>
					<TextField
						margin="normal"
						fullWidth
						id="username"
						label="Username"
						name="Username"
						autoComplete="Username"
						defaultValue={name}
						onChange={handleNameChange}
					/>
					<TextField
						margin="normal"
						fullWidth
						id="picture"
						label="Picture"
						name="Picture"
						autoComplete="Picture url"
						defaultValue={profilePicture}
						onChange={handlePictureUrlChange}
					/>
					<TextField
						margin="normal"
						fullWidth
						name="workStatus"
						label="Work Status"
						id="workStatus"
						multiline
						defaultValue={workStatus}
						onChange={handleStatusChange}
					/>
					<TextField
						margin="normal"
						fullWidth
						name="bio"
						label="Bio or Short Resume"
						id="bio"
						multiline
						rows={5}
						defaultValue={bio}
						onChange={handleBioChange}
					/>
					<TextField
						margin="normal"
						fullWidth
						name="funFact"
						label="Fun Fact"
						multiline
						id="funFact"
						defaultValue={funFact}
						onChange={handleFactChange}
					/>
					<Button
						onClick={handleUpdateUser}
						fullWidth
						disabled={requiredInputsFilled}
						variant="contained"
						sx={{ mt: 3, mb: 2 }}>
						Save changes
					</Button>
				</Box>
				{message && <Typography>{message}</Typography>}
			</Box>
		</Container>
	);
};

export default UserSettings;
