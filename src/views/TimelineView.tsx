import { Grid, IconButton, Tooltip } from "@mui/material";
import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import SearchBar from "../common/Searchbar";
import {
	selectActiveEvent,
	setEventStatus,
} from "../features/event/eventSlice";
import { selectActivePostId, setPostsStatus } from "../features/post/postSlice";
import EventThread from "../features/thread/EventThread";
import PostThread from "../features/thread/PostThread";
import DatagridTimeline from "../features/timeline/DatagridTimeline";
import Timeline from "../features/timeline/Timeline";
import ReplayRoundedIcon from "@mui/icons-material/ReplayRounded";
import { DataStatus } from "../common/commonTypes";
import withAuth from "../hoc/withAuth";
import { useNavigate } from "react-router-dom";
import {
	setAllUsersStatus,
	setCurrentUserId,
} from "../features/allUsers/usersSlice";

/**
 * The timeline view. Handles the timeline list and the thread(s)
 * @returns
 */
const TimelineView = () => {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const [gridView, setGridView] = useState(false);

	const activePost = useAppSelector(selectActivePostId);
	const activeEvent = useAppSelector(selectActiveEvent);

	const activeThread = activePost || activeEvent;

	const handleSwitchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setGridView(event.target.checked);
	};

	const handleReloadClick = () => {
		dispatch(setPostsStatus(DataStatus.Idle));
		dispatch(setEventStatus(DataStatus.Idle));
		dispatch(setAllUsersStatus(DataStatus.Idle));
	};

	const handleUsernameClick = (userId: string) => {
		dispatch(setCurrentUserId(userId));
		navigate("/profile");
	};

	return (
		<Grid container justifyContent="center">
			<Grid
				xs={activeThread ? 12 : 12}
				lg={activeThread ? 6 : 12}
				xl={activeThread ? 5 : 12}
				item
				justifyContent="center"
				sx={{ mr: 2 }}>
				<Grid container justifyContent="center" marginBottom={2}>
					<SearchBar />
					<Tooltip title="Update" placement="right">
						<IconButton onClick={handleReloadClick} sx={{ ml: 4 }} size="large">
							<ReplayRoundedIcon fontSize="large" />
						</IconButton>
					</Tooltip>
				</Grid>
				{/*<Grid container justifyContent="center">
					<Switch checked={gridView} onChange={handleSwitchChange}></Switch>
				</Grid>*/}
				{gridView ? <DatagridTimeline></DatagridTimeline> : <Timeline />}
			</Grid>
			{activeThread && (
				<Grid
					xs={activeThread ? 8 : 0}
					lg={activeThread ? 5 : 0}
					xl={activeThread ? 6.5 : 0}
					item>
					{activePost && (
						<PostThread
							handleUsernameClick={handleUsernameClick}
							id={activePost}
						/>
					)}
					{activeEvent && (
						<EventThread
							handleUsernameClick={handleUsernameClick}
							id={activeEvent}
						/>
					)}
				</Grid>
			)}
		</Grid>
	);
};

export default withAuth(TimelineView);
