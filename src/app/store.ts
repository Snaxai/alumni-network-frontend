import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import postReducer from "../features/post/postSlice";
import loginReducer from "../features/login/loginSlice";
import userReducer from "../features/user/userSlice";
import calendarReducer from "../features/calendar/calendarSlice";
import allUsersReducer from "../features/allUsers/usersSlice";
import topicReducer from "../features/topic/topicSlice";
import groupReducer from "../features/group/groupSlice";
import eventReducer from "../features/event/eventSlice";
import searchQueryReducer from "../common/searchQuerySlice";
import rsvpReducer from "../features/rsvp/rsvpSlice";

/**
 * Combining all slices to a single store.
 * This is an abstraction over the standard redux createStore() function
 * Returns a configured Redux store
 */
export const store = configureStore({
	reducer: {
		posts: postReducer,
		login: loginReducer,
		user: userReducer,
		calendar: calendarReducer,
		allUsers: allUsersReducer,
		topic: topicReducer,
		group: groupReducer,
		event: eventReducer,
		search: searchQueryReducer,
		rsvp: rsvpReducer,
	},
});

// Exports types used throughout the app
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
